const server = require("http").createServer();
const { v4 } = require("uuid");
const io = require("socket.io")(server);
const Redis = require("ioredis");

const redisConfig = {
  db: 1,
};
const redis = new Redis(redisConfig);
const turnTimeout = 10;

const generateGameData = () => {
  return {
    won: "",
    status: "pending",
    turn: "player1",
    positions: {},
  };
};

const subScribeExpired = () => {
  const subscriber = new Redis(redisConfig);
  const expired_subKey = `__keyevent@${redisConfig.db}__:expired`;
  subscriber.subscribe(expired_subKey, () => {
    subscriber.on("message", (_, key) => {
      const gameId = key.split("_")[0];
      setTurn(gameId);
    });
  });
};

redis.send_command(
  "config",
  ["set", "notify-keyspace-events", "Ex"],
  subScribeExpired
);

const getConnectedPlayers = (roomId) => {
  return io.in(roomId).allSockets();
};

const setTurn = async (gameId) => {
  const players = await getConnectedPlayers(gameId);
  if (!players.size || players.size < 2) return;
  const turnTimeoutKey = `${gameId}_turn`;
  const turn = await redis.hget(gameId, "turn");
  if (!turn) {
    return;
  }
  const turnTime = turn === "player1" ? "player2" : "player1";
  await redis.setex(turnTimeoutKey, turnTimeout, turnTime);
  await redis.hset(gameId, "turn", turnTime);
  io.in(gameId).emit("turn-changed", turnTime);
};

const checkForWinners = (parsedPositions) => {
  let winner;
  if (
    parsedPositions[1] === parsedPositions[2] &&
    parsedPositions[2] === parsedPositions[3] &&
    parsedPositions[3] === parsedPositions[1]
  ) {
    winner = parsedPositions[1];
  } else if (
    parsedPositions[1] === parsedPositions[4] &&
    parsedPositions[4] === parsedPositions[7] &&
    parsedPositions[7] === parsedPositions[1]
  ) {
    winner = parsedPositions[1];
  } else if (
    parsedPositions[1] === parsedPositions[5] &&
    parsedPositions[5] === parsedPositions[9] &&
    parsedPositions[9] === parsedPositions[1]
  ) {
    winner = parsedPositions[1];
  } else if (
    parsedPositions[4] === parsedPositions[5] &&
    parsedPositions[5] === parsedPositions[6] &&
    parsedPositions[6] === parsedPositions[4]
  ) {
    winner = parsedPositions[4];
  } else if (
    parsedPositions[7] === parsedPositions[8] &&
    parsedPositions[8] === parsedPositions[9] &&
    parsedPositions[9] === parsedPositions[7]
  ) {
    winner = parsedPositions[7];
  } else if (
    parsedPositions[2] === parsedPositions[5] &&
    parsedPositions[5] === parsedPositions[8] &&
    parsedPositions[8] === parsedPositions[2]
  ) {
    winner = parsedPositions[2];
  } else if (
    parsedPositions[3] === parsedPositions[6] &&
    parsedPositions[6] === parsedPositions[9] &&
    parsedPositions[9] === parsedPositions[3]
  ) {
    winner = parsedPositions[3];
  } else if (
    parsedPositions[3] === parsedPositions[5] &&
    parsedPositions[5] === parsedPositions[7] &&
    parsedPositions[7] === parsedPositions[3]
  ) {
    winner = parsedPositions[3];
  }
  if (Object.keys(parsedPositions).length >= 9 && !winner) {
    winner = "draw";
  }
  return winner;
};

io.on("connection", (socket) => {
  function handleError(message) {
    socket.emit("error", message);
  }

  socket.on("new-game", async () => {
    const gameId = v4();
    const gameData = generateGameData();
    for (const key in gameData) {
      await redis.hset(
        gameId,
        key,
        typeof gameData[key] === "object"
          ? JSON.stringify(gameData[key])
          : gameData[key]
      );
    }
    socket.emit("game-created", gameId);
  });

  socket.on("join-game", async ({ gameId }) => {
    const game = await redis.exists(gameId);
    if (!game) {
      return handleError("game does not exist");
    }
    const gameStatus = await redis.hget(gameId, 'status')
    if(gameStatus === 'completed') {
      return handleError('Game has been already completed')
    }
    socket.join(gameId);
    const players = await getConnectedPlayers(gameId);
    if (players.size === 2) {
      await redis.setex(`${gameId}_turn`, turnTimeout, "player1");
      io.in(gameId).emit("game-start", true);
      io.in(gameId).emit("turn-changed", "player1");
    }
  });

  socket.on("click", async (data) => {
    const isConnectedToRoom = socket.rooms.has(data.gameId);

    if (!isConnectedToRoom) {
      return handleError("Player is not associated with this game");
    }

    const { gameId, position } = data;

    if (position < 1 && position > 9) {
      return handleError("Given position is invalid");
    }

    const gameData = await redis.hgetall(gameId);

    if(gameData.status === 'completed') {
      return handleError("Game has been already completed")
    }

    const turn = gameData.turn;
    let parsedPositions = JSON.parse(gameData.positions);

    if (parsedPositions && !parsedPositions[position]) {
      await redis.expire(`${gameId}_turn`, 0);

      parsedPositions[position] = turn === "player1" ? "circle" : "cross";
      await redis.hset(gameId, "positions", JSON.stringify(parsedPositions));
    } else {
      handleError("Position is already occupied");
    }
    const winner = checkForWinners(parsedPositions);
    if (winner) {
      await redis.expire(`${gameId}_turn`, 0);
      await redis.hset(gameId, "status", "completed");
      await redis.hset(gameId, "won", winner);
      io.to(gameId).emit("game-complete", winner);
      return;
    } else {
      setTurn(gameId);
    }
  });
  
});

const PORT = process.env.PORT || 4000;

server.listen(PORT, () => {
  console.log(`running @ ${PORT}`);
});

// Evetns to listen on postman / frontend
// game-created
// game-start
// game-complete
// turn-changed
// error
